module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "airbnb"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "import/no-named-as-default": "off",
        "import/no-named-as-default-member": "off",
        "no-plusplus": "off",
        "react/jsx-props-no-spreading": "off",
        "react/forbid-prop-types": "off"
    }
};
import React, { useState } from 'react';
import { Button } from "../../atomary";
import clsx from "clsx";
import './button_like.sass';

const ButtonLike = ({ number = 0, className }) => {
    const [clicked, setClicked] = useState(false);
    const [count, setCount] = useState(number);

    const heart = clicked ?
        <i key='heart_clicked' className='material-icons heart'>
            favorite
        </i> :
        <i key='heart' className='material-icons heart'>
            favorite_border
        </i>
        ;

    const clickHandler = (v) => {
        const add = v ? 1 : -1

        setClicked(v)
        setCount(count + add);
    };

    return (
        <Button
            key='button_like'
            className={clsx(
                'button_like',
                clicked && 'button_like_clicked',
                className
            )}
            onClick={() => clickHandler(!clicked)}
            withIcon
            icon={heart}
            iconPosition='left'
            textVariant='body'
        >
            {count}
        </Button>
    );
};

export default ButtonLike;

import React from 'react';
import {TextFieldDefault} from "../index";
import {Button} from "../../atomary";

const TextFieldSubscription = () => {
    const button = (
        <Button
            withIcon
            circle
            variant='clearPrimary'
            withoutText
        />
    );

    return (
        <TextFieldDefault
            withButton
            button={button}
        />
    );
};

export default TextFieldSubscription;

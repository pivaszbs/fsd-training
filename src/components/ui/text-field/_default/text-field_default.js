import React from 'react';
import { TextField } from "../../atomary";
import './text-field_default.sass';

const TextFieldDefault = (props) => {
    return (
        <TextField className={'text-field_default' + props.className} placeholder={'email'} {...props} />
    );
};

export default TextFieldDefault;

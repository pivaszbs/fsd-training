import React, { useState } from 'react';
import TextFieldDefault from "../../_default/text-field_default";

const TextFieldMaskedDate = ({ className, ...others }) => {
    const [value, setValue] = useState('');

    const mask = (e) => {
        const v = e.target.value.replace(/\D/g, '').slice(0, 10);
        if (v.length >= 5) {
            setValue(`${v.slice(0, 2)}.${v.slice(2, 4)}.${v.slice(4)}`);
        }
        else if (v.length >= 3) {
            setValue(`${v.slice(0, 2)}.${v.slice(2)}`);
        } else {
            setValue(v);
        }
    };

    return (
        <TextFieldDefault
            onChange={mask}
            placeholder='dd.mm.yyyy'
            value={value}
            maxLength='10'
            className={className}
            {...others}
        />
    );
};

export default TextFieldMaskedDate;

import React from 'react';
import { DropdownDefault, Row, Button } from "../../../index";
import './dropdown_with_apply.sass';

const DropdownWithApply = () => {
    const row = (
        <Row className='dropdown_with_apply'>
            <Button variant='clearPrimary'>
                Применить
            </Button>
        </Row>
    );

    return (
        <DropdownDefault addRows={[row]}/>
    );
};

export default DropdownWithApply;

import React from 'react';
import { DropdownDefault, Row, Button } from "../../../index";
import './dropdown_with_clear-and-apply.sass';

const DropdownWithClearAndApply = () => {
    const row = (
        <Row className='dropdown_with_clear-and-apply'>
            <Button variant='clearGray'>
                Очистить
            </Button>
            <Button variant='clearPrimary'>
                Применить
            </Button>
        </Row>
    );

    return (
        <DropdownDefault addRows={[row]}/>
    );
};

export default DropdownWithClearAndApply;

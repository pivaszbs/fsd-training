import React from 'react';
import './dropdown_default.sass';
import { Dropdown } from "../../atomary";

const items = [
    { text : 'спальни', number: 2 },
    { text : 'кровати', number: 2 },
    { text : 'ванные комнаты', number: 0 }
];

const DropdownDefault = ({ addRows }) => {

    return (<Dropdown
        className={'dropdown_default'}
        textProps={{
            className: 'dropdown_default__text',
            variant: 'body',
            placeholder: '2 спальни, 2 кровати...'
        }}
        items={items}
        addRows={addRows}
    />);
};

export default DropdownDefault;

import React from 'react';
import PropTypes from 'prop-types';

import { Row, Column, Text } from '../atomary';
import './advantage.sass';
import Divider from '../atomary/divider';

function Advantage({ icon, name, description }) {
  return (
    <div className="advantage">
      <Row className="advantage__row">
        {icon}
        <Column>
          <Text className="advantage__name">{name}</Text>
          <Text className="advantage__dexcription">{description}</Text>
        </Column>
      </Row>
      <Divider className="advantage__divider" />
    </div>
  );
}

Advantage.propTypes = {
  icon: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
};

Advantage.defaultProps = {
  description: '',
};

export default Advantage;

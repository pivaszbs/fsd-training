import React from 'react'
import Advantage from '../advantage';

function AdvantageComfort() {
    const icon = (
        <i className='material-icons advantage__icon'>
            insert_emoticon
        </i>
    );

    return <Advantage icon={icon} name={'Комфорт'} description={'Шумопоглощающие стены'} />
}

export default AdvantageComfort

import Advantage from "./advantage";
import AdvantageComfort from "./__comfort/advantage__comfort";
import AdvantageConvenience from "./__convenience/advantage__convenience";

export {
    Advantage,
    AdvantageComfort,
    AdvantageConvenience
}
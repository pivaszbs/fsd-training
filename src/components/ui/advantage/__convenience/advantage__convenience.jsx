import React from 'react'
import Advantage from '../advantage';

function AdvantageConvenience() {
    const icon = (
        <i className='material-icons advantage__icon'>
            location_city
        </i>
    );

    return <Advantage icon={icon} name='Комфорт' description='Шумопоглощающие стены' />
}

export default AdvantageConvenience

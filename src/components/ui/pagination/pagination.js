import React from 'react';
import { Button } from '../atomary';

const Pagination = () => {
    return (
        <div className='pagination'>
            <Button variant='primary' circle>1</Button>
            <Button variant='clearGray' circle>2</Button>
            <Button variant='clearGray' circle>3</Button>
            <Button variant='clearGray' circle>...</Button>
            <Button variant='clearGray' circle>15</Button>
            <Button variant='green' withIcon circle>15</Button>
        </div>
    );
};

export default Pagination;

import React from 'react'
import { Row, Column, Text, Avatar } from '../atomary';
import ButtonLike from '../button';
import './feedback.sass';

function Feedback({ name, timeago, text }) {
    return (
        <Column className='feedback'>
            <Row>
                <Avatar className='feedback__avatar' />
                <Column className='feedback__info'>
                    <Text variant='body' className='feedback__name'>{name}</Text>
                    <Text variant='body' className='feedback__timeago'>{timeago}</Text>
                </Column>
            </Row>
            <Row>
                <ButtonLike />
                <Text className='feedback__text feedback__info'>{text}</Text>
            </Row>
        </Column>
    )
}

export default Feedback

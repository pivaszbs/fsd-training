import React from 'react';
import PropTypes from 'prop-types';

const DropdownControls = ({ children }) => (
  <div className="dropdown__controls">
    {children}
  </div>
);

DropdownControls.propTypes = {
  children: PropTypes.node.isRequired,
};

export default DropdownControls;

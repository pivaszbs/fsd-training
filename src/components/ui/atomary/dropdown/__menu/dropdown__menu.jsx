import React from 'react';
import PropTypes from 'prop-types';

import TextField from '../../text-field';
import DropdownExpand from '../__expand/dropdown__expand';

const DropdownMenu = ({ className, icon, ...other }) => (
  <div className="dropdown__menu">
    <TextField
      className={className}
      disabled
      withButton
      button={<DropdownExpand icon={icon} />}
      {...other}
    />
  </div>
);

DropdownMenu.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.node,
  other: PropTypes.object,
};

DropdownMenu.defaultProps = {
  className: '',
  icon: null,
  other: {},
};

export default DropdownMenu;

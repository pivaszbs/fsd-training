import React from 'react';
import './dropdown.sass';
import clsx from 'clsx';
import DropdownRow from './__row/dropdown__row';
import DropdownMenu from './__menu';

const Dropdown = (props) => {
  const {
    items = [],
    textProps,
    rowProps,
    icon,
    className,
    addRows = [],
  } = props;

  const rows = items.map(({ text, number }) => (
    <DropdownRow text={text} number={number} {...rowProps} />
  ));

  return (
    <div className={clsx('dropdown', className)}>
      <DropdownMenu {...textProps} icon={icon} />
      {[...rows, ...addRows]}
    </div>
  );
};

Dropdown.propTypes = {
    items: PropTypes.array,
    textProps: PropTypes.object,
    rowProps: PropTypes.object,
    icon: PropTypes.node,
    className: PropTypes.string,
    addRows: PropTypes.array,
};

Dropdown.defaultProps = {
  items: [],
  textProps: {},
  rowProps: {},
  icon: null,
  className: '',
  addRows: []
};

export default Dropdown;

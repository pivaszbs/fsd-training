import React from 'react';
import PropTypes from 'prop-types';

import Button from '../../button';

const DropdownExpand = ({ icon }) => <Button className="dropdown__expand" variant="clearGray" circle withIcon icon={icon} />;

const defaultIcon = (
  <i className="material-icons">
        expand_more
  </i>
);

DropdownExpand.propTypes = {
  icon: PropTypes.node,
};

DropdownExpand.defaultProps = {
  icon: defaultIcon,
};

export default DropdownExpand;

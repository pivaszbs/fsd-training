import React from 'react';
import PropTypes from 'prop-types';

// eslint-disable-next-line import/no-cycle
import { Button, Row, Text } from '../../index';
import DropdownControls from '../__controls';

const DropdownRow = ({ text, number }) => {
  const minus = (
    <Button
      variant="clearGray"
      className="dropdown__button"
      circle
    >
        -
    </Button>
  );

  const plus = (
    <Button
      variant="clearGray"
      className="dropdown__button"
      circle
    >
        +
    </Button>
  );

  return (
    <Row className="dropdown__row">
      <Text variant="h3">
        {text}
      </Text>
      <DropdownControls>
        {minus}
        <Text variant="h3">
          {number}
        </Text>
        {plus}
      </DropdownControls>
    </Row>
  );
};

DropdownRow.propTypes = {
  text: PropTypes.string.isRequired,
  number: PropTypes.number,
};

DropdownRow.defaultProps = {
  number: 0,
};

export default DropdownRow;

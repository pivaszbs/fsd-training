import React from 'react';
import PropTypes from 'prop-types';

import './column.sass';
import clsx from 'clsx';

const Column = ({ children, className }) => (
  <div className={clsx('column', className)}>
    {children}
  </div>
);

Column.propTypes = {
  children: PropTypes.element.isRequired,
  className: PropTypes.string,
};

Column.defaultProps = {
  className: '',
};

export default Column;

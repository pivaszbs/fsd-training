
import React, { useState } from 'react';
import RangeSlider from './components/RangeSlider';
import './range-slider.sass';

// value={ start: 20, end: 80 }
const Slider = () => {
  const [value, setValue] = useState({
    start: 20,
    end: 100,
  });

  const onChange = (v) => setValue(v);

  return (
    <div className="slider-container">
      <RangeSlider
        value={value}
        onChange={onChange}
        min={20}
        max={100}
        step={5}
        wrapperClassName="slider"
        trackClassName="slider__track"
        highlightedTrackClassName="slider__track_highlighted"
        handleClassName="slider__handle"
      />
      {`${value.start} `}
      {value.end}
    </div>
  );
};

export default Slider;

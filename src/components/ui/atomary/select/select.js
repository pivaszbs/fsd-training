import React from 'react';
import './select.sass';

const Select = (props) => {
    const {
        placeholder = 'Select your options',
        items = []
    } = props;

    const options = items.map(item => (
            <option
                key={item.id}
                value={item.value}
            >
                {item.text}
            </option>
        ));

    return (
        <div className='select'>
            <select>
                <option disabled value='' selected>
                    {placeholder}
                </option>
                {options}
            </select>
        </div>
    );
};

export default Select;

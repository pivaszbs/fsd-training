import React from 'react';
import './text-field.sass';
import clsx from "clsx";
import TextFieldButton from "./__button/text-field__button";

const TextField = (props) => {
    const {
        placeholder,
        withButton,
        className,
        button,
        onButtonClick,
        ...other
    } = props;

    return (
        <div className={clsx(
            'text-field',
            className)
        }>
            <input
                placeholder={placeholder}
                {...other}
            />

            <TextFieldButton withButton={withButton} button={button} onClick={onButtonClick} />
        </div>
    );
};

export default TextField;

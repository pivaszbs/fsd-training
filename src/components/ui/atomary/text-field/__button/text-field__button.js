import React from "react";
import Button from "../../button";
import './text-field__button.sass';

const DefaultButton = ({ onClick }) => {
    const icon = (
        <i className='material-icons'>
            expand_more
        </i>
    );

    return <Button className='text-field__button' variant={'clearGray'} circle withoutText withIcon icon={icon} onClick={onClick} />
};

const TextFieldButton = ({ withButton, onClick, button = <DefaultButton onClick={onClick} /> }) => {
    if (withButton) {
        return (
            <div className='text-field__button'>{button}</div>
        )
    }

    return null;
};

export default TextFieldButton;

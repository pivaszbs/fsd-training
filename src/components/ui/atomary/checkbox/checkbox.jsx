import React from 'react';
import './checkbox.sass';
import PropTypes from 'prop-types';
import Text from '../text';

const Checkbox = ({ children }) => (
  <label className="checkbox">
    <input type="checkbox" />
    <Text className="checkbox__text">
      {children}
    </Text>
  </label>
);

Checkbox.propTypes = {
  children: PropTypes.string.isRequired,
};

export default Checkbox;

import React from 'react'
import { Row } from '../..';
import { TextFieldMaskedDate } from '../../../text-field';
import Text from '../../text';
import './datetime__range-picker.sass';
import Days from '../__days/datetime__days';
import clsx from 'clsx';

const ArrowBack = ({ onClick }) => (
    <i onClick={onClick} className='material-icons arrow_back'>
        arrow_back
    </i>
);

const monthes = ['Январь', "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Декабрь", "Январь"]

const dayOfTheWeek = ['Пн', 'Вт', "Ср", "Чт", "Пт", "Сб", "Вс"];

const ArrowForward = ({ onClick }) => (
    <i onClick={onClick} className='material-icons arrow_back arrow_forward'>
        arrow_forward
    </i>
);

const Month = ({ month }) => (
    <Text className='datetime__month' variant='h2'>{monthes[month]}</Text>
);

function DateTimeRangePicker(props) {
    const {
        openStart,
        openEnd,
        startDay,
        endDay,
        onChange,
        onStartFocus,
        onEndFocus,
        onMonthChange,
        diff
    } = props;

    const open = openStart || openEnd;
    
    let month;
    if (openEnd) {
        month = endDay.month() + diff;
    } else {
        month = startDay.month() + diff
    }

    return (
        <div className='datetime__range-picker'>
            <Row>
                <TextFieldMaskedDate
                    onButtonClick={onStartFocus}
                    onFocus={onStartFocus}
                    className='datetime__start-range'
                    value={startDay.format('DD.MM.YYYY')}
                    withButton
                />
                <TextFieldMaskedDate
                    onButtonClick={onEndFocus}
                    onFocus={onEndFocus}
                    className='datetime__end-range'
                    value={endDay.format('DD.MM.YYYY')}
                    withButton
                />
            </Row>
            <div className={clsx('datetime__popup', open ? 'datetime__popup_open' : 'datetime__popup_hidden')}>
                <Row className='datetime__head'>
                    <ArrowBack onClick={() => onMonthChange(-1)}/>
                    <Month month={month} />
                    <ArrowForward onClick={() => onMonthChange(1)} />
                </Row>
                <div className='datetime__body table'>
                    {dayOfTheWeek.map(item => (
                        <div key={item} className='datetime__day table__head'>
                            {item}
                        </div>
                    ))}
                    <Days start={openStart} end={openEnd} startDay={startDay} endDay={endDay} onChange={onChange} diff={diff} />
                </div>
            </div>
        </div>
    )
}

export default DateTimeRangePicker

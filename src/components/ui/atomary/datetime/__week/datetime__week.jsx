import React from 'react';
import PropTypes from 'prop-types';

const Week = ({ key, children }) => (
  <React.Fragment key={key}>
    {children}
  </React.Fragment>
);

Week.propTypes = {
  key: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default Week;

/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';

import './datetime__day.sass';

const Day = (props) => {
  const {
    key, className, currentDate, onClick, 'data-value': data,
  } = props;

  return (
    <div className={className} key={key} data-value={data} onClick={onClick}>
      {currentDate.date()}
    </div>
  );
};

Day.propTypes = {
  key: PropTypes.string.isRequired,
  className: PropTypes.string,
  currentDate: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  'data-value': PropTypes.number.isRequired,
};

Day.defaultProps = {
  className: '',
};

export default Day;

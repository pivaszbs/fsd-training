import React from 'react';
import moment from 'moment';
import Day from '../__day/datetime__day';
import Week from '../__week/datetime__week';

const alwaysValidDate = () => true;

const Days = (props) => {
  let selected;

  const {
    start, end, startDay, endDay, diff,
  } = props;

  if (start) {
    selected = startDay.clone();
  } else if (end) {
    selected = endDay.clone();
  } else {
    selected = moment.utc();
  }

  const date = selected.startOf('month').add(diff, 'month');
  const prevMonth = date.clone().subtract(1, 'months');
  const currentYear = date.year();
  const currentMonth = date.month();
  const weeks = [];
  let days = [];
  const isValid = props.isValidDate || alwaysValidDate;
  let classes;
  let isDisabled;
  let dayProps;
  let currentDate;

  const updateSelectedDate = (e) => {
    const target = e.currentTarget;
    let modifier = 0;
    const viewDate = date;
    currentDate = (selected && selected.clone()) || date;

    let innerDate;

    if (target.className.indexOf('datetime__day') !== -1) {
      if (target.className.indexOf('datetime__day_new') !== -1) { modifier = 1; } else if (target.className.indexOf('datetime__day_old') !== -1) { modifier = -1; }

      innerDate = viewDate.clone()
        .month(viewDate.month() + modifier)
        .date(parseInt(target.getAttribute('data-value'), 10));
    } else if (target.className.indexOf('datetime__month') !== -1) {
      innerDate = viewDate.clone()
        .month(parseInt(target.getAttribute('data-value'), 10))
        .date(currentDate.date());
    }


    props.onChange(innerDate, start, end);
  };

  // Go to the last week of the previous month
  prevMonth.date(prevMonth.daysInMonth()).startOf('week');
  const lastDay = prevMonth.clone().add(42, 'd');

  while (prevMonth.isBefore(lastDay)) {
    classes = 'datetime__day table__item';
    currentDate = prevMonth.clone();

    if ((prevMonth.year() === currentYear && prevMonth.month() < currentMonth) || (prevMonth.year() < currentYear)) { classes += ' datetime__day_old'; } else if ((prevMonth.year() === currentYear && prevMonth.month() > currentMonth) || (prevMonth.year() > currentYear)) { classes += ' datetime__day_new'; }

    if (prevMonth.isSame(startDay, 'day')) {
      classes += ' datetime__day_start';
      classes += ' datetime__day_active';
    }

    if (prevMonth.isSame(endDay, 'day')) {
      classes += ' datetime__day_end';
      classes += ' datetime__day_active';
    }

    if (prevMonth.isAfter(startDay) && prevMonth.isBefore(endDay)) {
      classes += ' datetime__range';
    }

    if (prevMonth.isSame(moment(), 'day')) { classes += ' datetime__day_today'; }

    isDisabled = !isValid(currentDate, selected);
    if (isDisabled) { classes += ' datetime__day_disabled'; }

    dayProps = {
      key: prevMonth.format('M_D'),
      'data-value': prevMonth.date(),
      className: classes,
      currentDate,
      selected,
    };

    if (!isDisabled) { dayProps.onClick = updateSelectedDate; }

    days.push(<Day {...dayProps} />);

    if (days.length === 7) {
      weeks.push(<Week key={prevMonth.format('M_D')}>{days}</Week>);
      days = [];
    }

    prevMonth.add(1, 'd');
  }

  return weeks;
};

export default Days;

import React, { Component } from 'react';
import moment from 'moment';
import withClickOutside from 'react-click-outside';
import DateTimeRangePicker from './__range-picker/datetime__range-picker';
import './datetime.sass';

class MyDateTime extends Component {
  state = {
    openStart: false,
    openEnd: false,
    startDay: moment.utc(),
    endDay: moment.utc(),
    diff: 0
  }

  onMonthChange = (d) => {
    const { diff } = this.state;
    this.setState({ diff: diff + d })
  };

  close = () => {
    this.setState({
      diff: 0,
      openEnd: false,
      openStart: false
    });
  };

  handleClickOutside() {
    this.close();
  }

  onChange = (date, start, end) => {
    if (start) {
      this.setState({ startDay: date });
    } else if (end) {
      this.setState({ endDay: date })
    }
    this.close();
  };

  onStartFocus = () => {
    const { openStart } = this.state;
    if (openStart) {
      this.close();
    } else {
      this.setState({
        openStart: true,
        openEnd: false
      })
    }
  };

  onEndFocus = () => {
    const { openEnd } = this.state;

    if (openEnd) {
      this.close();
    } else {
      this.setState({
        openStart: false,
        openEnd: true
      });
  };
}

  render() {
    const {
      openStart,
      openEnd,
      startDay,
      endDay,
      diff,
    } = this.state;

    const {
      onChange,
      onStartFocus,
      onEndFocus,
      onMonthChange,
    } = this;

    const rangeProps = {
      openStart,
      openEnd,
      startDay,
      endDay,
      onChange,
      onStartFocus,
      onEndFocus,
      onMonthChange,
      diff,
    };

    return (
      <DateTimeRangePicker {...rangeProps} />
    );
  }

};

export default withClickOutside(MyDateTime);

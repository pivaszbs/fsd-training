import React from 'react';
import './divider.sass';
import clsx from 'clsx';
import PropTypes from 'prop-types';


function Divider({ className }) {
  return (
    <div className={clsx('divider', className)} />
  );
}

Divider.propTypes = {
  className: PropTypes.string,
};

Divider.defaultProps = {
  className: '',
};

export default Divider;

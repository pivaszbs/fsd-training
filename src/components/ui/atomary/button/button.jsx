import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';

import Text from '../text';
import './button.sass';

const variantMapping = {
  primary: 'button_primary',
  secondary: 'button_secondary',
  clearPrimary: 'button_clear-primary',
  clearGray: 'button_clear-gray',
  long: 'button_primary button_long',
  green: 'button_green',
};

const defaultIcon = () => (
  <i className="material-icons arrow">
    arrow_forward
  </i>
);

const Button = (props) => {
  const {
    children,
    variant,
    transparent,
    circle,
    iconPosition = 'right',
    className,
    textVariant = 'h3',
    ...other
  } = props;

  let {
    icon = defaultIcon(),
    withIcon,
    inline,
    withoutText,
  } = props;

  const inner = () => {
    if (variant === 'long') {
      inline = true;
      withIcon = true;
      withoutText = true;
    }
    icon = withIcon && icon;
    if (withoutText) {
      return icon;
    }
    const text = (
      <Text key="button__text" inline={inline} variant={textVariant} className="button__text">{children}</Text>
    );

    const order = [];
    if (iconPosition === 'left') {
      order.push(
        icon,
        text,
      );
    } else {
      order.push(text, icon);
    }
    return order;
  };

  return (
    <button
      type="button"
      className={clsx(
        'button',
        variantMapping[variant],
        transparent && 'button_transparent',
        circle && 'button_circle',
        className,
      )}
      {...other}
    >
      {inner()}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node,
  variant: PropTypes.string.isRequired,
  transparent: PropTypes.bool,
  circle: PropTypes.bool,
  iconPosition: PropTypes.string,
  className: PropTypes.string,
  textVariant: PropTypes.string,
  icon: PropTypes.element,
  withIcon: PropTypes.bool,
  withoutText: PropTypes.bool,
  inline: PropTypes.bool,
};

Button.defaultProps = {
  children: null,
  transparent: false,
  circle: false,
  iconPosition: 'right',
  className: '',
  textVariant: 'body',
  icon: null,
  withIcon: false,
  withoutText: false,
  inline: false,
};

export default Button;

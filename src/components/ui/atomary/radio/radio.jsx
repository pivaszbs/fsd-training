import React, { useState } from 'react';
import './radio.sass';
import clsx from 'clsx';
import Text from '../text';

const Radio = ({ children }) => {
  const [checked, setChecked] = useState(false);

  return (
    <label className={clsx(
      'radio',
      checked && 'radio_checked'
    )}
    >
      <input onChange={() => setChecked(true)} type="radio" />
      <Text className="radio__text">{children}</Text>
    </label>
  );
};

export default Radio;

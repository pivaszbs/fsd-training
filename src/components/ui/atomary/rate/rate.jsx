import React from 'react';
import PropTypes from 'prop-types';

import Row from '../row';
import './rate.sass';

const Star = ({ picked, index, setCount }) => {
  const star = (
    <i className="material-icons star" onFocus={() => setCount(index + 1)} key={index}>
        star
    </i>
  );

  const borderedStar = (
    <i className="material-icons star star_border" key={index} onFocus={() => setCount(index + 1)}>
        star_border
    </i>
  );

  return picked ? star : borderedStar;
};

function Rate({ max = 5, count = 0, setCount = () => {} }) {
  const stars = [];

  for (let i = 0; i < max; i++) {
    if (i < count) {
      stars.push(<Star picked index={i} setCount={setCount} />);
    } else {
      stars.push(<Star index={i} setCount={setCount} />);
    }
  }

  return (
    <Row className="rate">
      {stars}
    </Row>
  );
}

Rate.propTypes = {
  max: PropTypes.number,
  count: PropTypes.node,
  setCount: PropTypes.func,
};

Rate.defaultProps = {
  max: 5,
  count: 0,
  setCount: () => {},
};

export default Rate;

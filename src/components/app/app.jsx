import React from 'react';

import {
  DateTime,
  RangeSlider
} from '../ui';

function App() {
  return (
    <div className="App">
      <DateTime />
      <RangeSlider />
    </div>
  );
}

export default App;

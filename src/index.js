import React from 'react';
import ReactDOM from 'react-dom';

import './styles/global-styles.sass';
import './styles/globals.sass';
import App from './components/app';

ReactDOM.render(<App />, document.getElementById('root'));
